<!--

# The issue title should be: UX onboarding (NAME), as (ROLE, STAGE GROUP)

-->

/assign (FILL IN WITH @ NEW TEAM MEMBER HANDLE) (FILL IN WITH @ UX BUDDY HANDLE)
/due (FILL IN WITH 1 MONTH AFTER START DATE)
/label ~onboarding 
/confidential

# Hello :wave: 

Welcome to the UX department (FILL IN WITH @ NEW TEAM MEMBER HANDLE)! We are happy to have you and support you! :slight\_smile:

To get you up to speed so you can start contributing to the team efforts and improving GitLab as a product, this onboarding issue will try to provide a few tips on how to navigate around the team resources as well as company resources.

First, you are likely to want to know where your place is in the company structure. For this, you would want to check out the [team chart](https://about.gitlab.com/team/chart/).

If you have questions, don't hesitate to mention (FILL IN WITH @ UX BUDDY HANDLE) (your [UX buddy](https://about.gitlab.com/handbook/engineering/ux/uxdesigner-onboarding/#ux-design-buddy)), (FILL IN WITH @ UX MANAGER HANDLE) (your UX Manager), or the whole UX department by using `@gitlab\-com/gitlab\-ux` (use the latter sparingly unless it's something important). You can also reach out to people in the [#ux Slack channel](https://gitlab.slack.com/messages/C03MSG8B7/).

You'll have a weekly 1:1 call with your UX buddy until your onboarding is complete! Fridays are great for these to talk through questions and set expectations for the next week. You onboarding doesn't have a scheduled end date, just focus on learning and getting comfortable with GitLab and the UX department.

## Company :earth\_africa:

Most of the information you will ever need is listed in your general onboarding document. However, there is a lot of things to digest in there so let's highlight a few items that you should focus on.

- [ ] The [handbook](https://about.gitlab.com/handbook/) is a great resource for all things GitLab. Understanding the [Values](https://about.gitlab.com/handbook/values/) is really important as that sets the tone for all interactions that you will have in the company. If anything is unclear about it, feel free to ask a question in this issue or in your next 1:1 with your UX buddy or UX manager.
- [ ] [Learn how we communicate as a company](https://about.gitlab.com/handbook/communication/). We use asynchronous communication as a start and are as open as we can be by communicating through public issues, chat channels, and placing an emphasis on ensuring that conclusions of offline conversations are written down. 
- [ ] Sooner or later you will have some money related questions, so be sure to check out the [spending company money page](https://about.gitlab.com/handbook/spending-company-money/).
- [ ] Finally, the [general guidelines](https://about.gitlab.com/handbook/general-guidelines/) will help you understand how we get things done.

Of course, it would be great to go through the whole handbook but that is going to be really difficult given the size of it so don't feel bad if you can't read through all of it. Remember that you can always search! For now, though, you should get to know your team a bit better.

## UX Department :raised\_hands:

Each department has it's own handbook section, and UX is no exception:
- [ ] Read through the whole [UX handbook](https://about.gitlab.com/handbook/engineering/ux/) section, please!
   - Consider this your first team task, understanding the Mission, Vision and how work is executed within the department. If you find any typo's or items that could be made clearer, please consider [editing that page](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/source/handbook/engineering/ux/index.html.md) by submitting a merge request. 
- [ ] Schedule [coffee-chats](https://about.gitlab.com/company/culture/all-remote/#coffee-chats) with other members of the UX department to get to know who you work with, and talk about everyday things. We want you to make friends and build relationships with the people you work with to create a more comfortable, well-rounded environment. 

To highlight some other important places:

- [ ] Check out the [UX department workflow](https://about.gitlab.com/handbook/engineering/ux/ux-department-workflow) which is important to get to know which dates matter and to align yourself to our development rhythm.
- [ ] Check out the [design project and documentation](https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md). Verify that you have access to the tools listed (for now mainly Sketch). And ask about any other tools you might need.
- [ ] Become familiar with our [design system](http://design.gitlab.com) which is the place where we document all product design decisions and things to help us make those decisions, like [personas](https://design.gitlab.com/research/personas).

You can [open up an issue at the design repository](https://gitlab.com/gitlab-org/gitlab-design/issues) to start a discussion for any design related matters. For more product related matters the [GitLab Community Edition issue tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues) would be the correct place!

## Staying informed :mega:

There is a lot of information flowing around, but to stay on top of the most important things you should be a part of [#general](https://gitlab.slack.com/messages/C0259241C), [#handbook](https://gitlab.slack.com/messages/C81PT2ALD), [#ux](https://gitlab.slack.com/messages/C03MSG8B7), and [#design-system](https://gitlab.slack.com/messages/CDNNDD1T3) channels in Slack. You are likely to have received an invite for the `Company call` (every day but Friday), but don't feel bad if you can't attend it. Just checking the [Company call agenda](https://docs.google.com/document/d/1JiLWsTOm0yprPVIW9W-hM4iUsRxkBt_1bpm3VXV4Muc/edit) should be enough to keep you on top of the most important events.

As for the weekly [UX meeting](https://docs.google.com/document/d/189WZO7uTlZCznzae2gqLqFn55koNl3-pHvU-eVnvG9c/edit) (every Tuesday), please make sure you are invited. Note, that you are not obligated to be part of any of the meetings that you are invited to. However, please note your absence by responding to invites in time. Not attending the meeting is no excuse for not reading the agenda items or going through the notes of the meeting. Oh, and did I mention there are [recordings available](https://drive.google.com/drive/folders/1pvtIfMFOj8Bf1pM3Ac-gYuG6pqtnNu7O?usp=sharing)? :slight\_smile: 

Once you are added to the UX Calendar on Google, attend to one of our `UX Hangouts` calls. We meet twice a week to hang out with other UX members and talk about anything we want.

Lastly, every department and stage group host so-called [group conversations](https://about.gitlab.com/handbook/people-operations/group-conversations/) to keep everyone informed about their efforts. Please make sure you have access to the `GitLab Team Meetings` calendar to be able to join any one of those. They are called conversations for a reason, meaning you are able to ask questions and join the discussion!

## Your product stage :fox:

**(FILL WITH PRODUCT STAGE NAME)** has been designated your product stage, to be your focus and responsibility. Any issues related to this stage will be labeled (FILL WITH PRODUCT STAGE ~ LABEL). Please make sure you:

- [ ] Understand our [product hierarchy](https://about.gitlab.com/handbook/product/categories/#hierarchy).
- [ ] Get to know your [stage's teammates](https://about.gitlab.com/handbook/product/categories/#devops-stages).
- [ ] Read its [vision](FILL WITH PRODUCT STAGE VISION PAGE URL).
- [ ] See where it stands in terms of [category maturity](https://about.gitlab.com/handbook/product/categories/maturity/).
- [ ] Start collaborating with your assigned UX Buddy. You should both arrange a day to have a regular 1:1 call (at least once a week). They have prepared a [Google Doc](FILL WITH GOOGLE DOC URL) to guide your sessions, feel free to add discussion points to the agenda.

<!--

Feel free to add more info and links that may help your new team mate
better understand the product stage, if necessary.

-->

In and around your third week you'll be asked by your UX buddy to shadow them for their daily UX activities. This is a good primer and a way to get more familiar with your product stage. You will serve as a second opinion and be included in meetings.

## Awesome flows :runner:

As part of your onboarding and to be best prepared for future responsibilities, it is recommended to walk through a few flows inside of the product, ask any questions you have (for example in this issue), and also document your steps as you go in a journal. This will give you a chance to reflect back on your onboarding and address/pinpoint issues that may have come up with a new sense of understanding.

Aside from adjusting towards the async workflow of GitLab please try to see if you can complete the following flows:
- [ ] Creating a project and launching a simple website on GitLab pages.
- [ ] Suggesting a change and opening a merge request towards any project.
- [ ] See how you receive notifications on issues. You get both emails as well as todos. (It's up to you to decide which one to use — this is a good conversation to have with your UX buddy).
- [ ] Create your `progress` folder in the [design project](https://gitlab.com/gitlab-org/gitlab-design) and commit your first design file.

<!--

Feel free to add specific product stage flows, if necessary.

-->

## Your onboarding experience :open\_hands: 

This issue is confidential by default, meaning this is a safe space to ask any questions and to describe any experiences you might have had. It's a tool to help **you** succeed, not others or something else!

To share some new found knowledge, the rest of the UX department would love to hear your experiences. You are in a position to help make things better for everyone (colleagues, new teammates, users, customers, etc.), including you.

1. Start discussions in this issue, one for each specific topic you want to share. These can be about the work itself, the product, the company, the team, or even about your personal side of it all (free time, health, relationships, etc.) To help you with this, here are some _suggestive and optional_ questions:
   - What were the big ideas you came across? The biggest learnings?
   - What were the big surprises?
   - What were the big questions?
   - What went well?
   - What did not go well?
   - What could be better?
1. Some topics will be questions, others statements, and others will be an opportunity to improve. Try to think about what could be improved and, if possible, how. In the end, each possible improvement should either:
   - start a separate issue for a broader discussion,
   - or start a merge request to change something, anything!
1. How can we improve the UX onboarding experience? Please keep notes on what went well, what did not go well, or could be improved. With that information, we'll iteratively improve our onboarding experience.
1. Before closing this issue, please add yourself to the next [UX weekly meeting agenda](https://docs.google.com/document/d/189WZO7uTlZCznzae2gqLqFn55koNl3-pHvU-eVnvG9c/) and spend 3-5 minutes max in total sharing a summary of your experience with the team.

## Closing thoughts :pray:

Taking in a lot of information can quickly feel overwhelming. This is normal and you are encouraged to ask questions any time. Embrace knowing nothing and you'll take it in as you go. Take your time to work through your onboarding tasks, you are not expected to be at 100% productivity right away! And remember: if you are here, it's because you are the best person we have found in the world (literally) to do this job :smile:

Working remotely comes with great power and great responsibility. [Here are a few tips on being efficient and productive](https://about.gitlab.com/2018/05/17/eliminating-distractions-and-getting-things-done/). 

### Remember to practice self-care

Make sure you take breaks if you feel overwhelmed or tired, especially if this is your first remote job. Remember stretch your legs, drink some water, and disconnect when necessary. Try to establish a healthy routine that will empower you to work in a way that enables you to deliver the best results.

---

# UX Buddy tasks
- [ ] Invite new team member to the product stage Slack channel.
- [ ] Add new team member to the product stage daily stand-up bot (your team uses this to post daily updates including what you are working on and if you have any blockers, much like a regular stand-up but much easier than doing one in real life). `*`
- [ ] Add new team member to the product stage [async retrospective](https://gitlab.com/gitlab-org/async-retrospectives). `*`
- [ ] Add new team member to the product stage weekly and monthly meetings. `*`
- [ ] Invite new team member to a 1:1 on their second week and repeating every Friday.
- [ ] Create a Google Doc for 1:1 agenda items ([template](https://docs.google.com/document/d/1sg4EtHBGTugxu-u2NSoH9LfE4zXT1ru1-Z3EiIXlohY)), share it with the new team member, attach it to the 1:1 event, and update the link

`*` Check with your Product Manager or PeopleOps if you need permission to add the new team member.
