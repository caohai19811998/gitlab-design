### Problem
<!-- What’s the problem that this icon solves? Why is it worth solving? --> 

### Solution
<!-- What’s the solution and why is it like that? Is the icon properly associated with the behavior it describes? Is there an existing icon in use that might conflict with this new one? Does the icon require any variants; for example, an outlined version, or versions showing status, new / opened / closed, etc,? -->

### Example(s)
<!-- One or more images showing the icon in use. -->

### Icon checklist

1. [ ] Read our [icon contribution guidelines](https://gitlab.com/gitlab-org/gitlab-design/blob/master/doc/pattern-library.md#icons).
1. [ ] Create a Sketch file in your progress folder just for this icon.
1. [ ] Ask a UXer to review your Sketch file, linking them to your latest commit so they know where to find it. If they have the capacity, they should assign themselves to this issue.
1. [ ] QA of your Sketch file by the reviewer.
1. [ ] Add your changes to the `pattern library` and `instance sheet`. When copying things over, watch out for duplicated symbols. Some of our [recommended plugins](https://gitlab.com/gitlab-org/gitlab-design/blob/master/CONTRIBUTING.md#plugins) can help with this.
1. [ ] QA of pattern library and instance sheet by the reviewer.
1. [ ] [Create an issue in the svgs library](https://gitlab.com/gitlab-org/gitlab-svgs/issues/new) to add the icon. Mark it as related to this issue.
1. [ ] Create a new MR and follow the necessary steps to add the icon in to the svgs repo. See the guidelines [here](https://gitlab.com/gitlab-org/gitlab-svgs#exporting-icons-from-sketch).
    1. [ ] Assign the MR to a maintainer of the project for review.
1. [ ] Add an agenda item to the next [UX weekly call](https://docs.google.com/document/d/189WZO7uTlZCznzae2gqLqFn55koNl3-pHvU-eVnvG9c/edit?usp=sharing) to inform everyone of the new icon. 

### Links / references

<!-- Add external links and references if necessary -->

/label ~"UX" ~"pattern library" ~"pajamas" ~"icon"

/cc @gitlab-com/gitlab-ux